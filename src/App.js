import React, { useState, useEffect } from "react";
import Modal from "./components/Modal";

const headers = [
  { label: "ทะเบียนรถยนต์", key: "ทะเบียนรถยนต์" },
  { label: "ยี่ห้อรถ", key: "ยี่ห้อรถ" },
  { label: "รุ่นรถ", key: "รุ่นรถ" },
  { label: "หมายเหตุ", key: "หมายเหตุ" },
];

const App = () => {
  const [allList, setAllList] = useState([]);
  const [carRegis, setCarRegis] = useState("");
  const [carBrand, setCarBrand] = useState("");
  const [carModel, setCarModel] = useState("");
  const [note, setNote] = useState("");
  const [showModal, setShowModal] = useState(false);

  const handleAddList = () => {
    let newListObj = {
      regis: carRegis,
      brand: carBrand,
      model: carModel,
      noted: note,
    };
    let updatedListArr = [...allList];
    updatedListArr.push(newListObj);
    setAllList(updatedListArr);
    localStorage.setItem("list", JSON.stringify(updatedListArr));
    setCarRegis("");
    setCarBrand("");
    setCarModel("");
    setNote("");
  };

  const handleDeleteList = (index) => {
    let reducedList = [...allList];
    reducedList.splice(index);
    localStorage.setItem("list", JSON.stringify(reducedList));
    setAllList(reducedList);
  };

  useEffect(() => {
    let savedList = JSON.parse(localStorage.getItem("list"));
    if (savedList) {
      setAllList(savedList);
    }
  }, []);

  return (
    <div className="bg-gradient-to-r from-cyan-700 to-blue-800 h-screen">
      <p className="text-7xl text-center text-yellow-300 py-10 font-serif">
        Company Car Information
      </p>
      <div className="flex justify-center items-center">
        <div className="bg-gradient-to-r from-cyan-500 to-blue-500 mt-10 w-fit rounded-xl">
          {" "}
          <div className="flex py-8 justify-between mr-14">
            <p className="text-5xl text-center text-gray-50 px-14 font-sans">
              ทะเบียนรถยนต์
            </p>
            <input
              type="text"
              className="py-3 px-5 rounded-md text-black outline-none text-2xl text-center"
              value={carRegis}
              onChange={(e) => setCarRegis(e.target.value)}
              placeholder="xxxxxxxxxx"
            />
          </div>
          <div className="flex justify-between mr-14">
            <p className="text-5xl text-center text-gray-50 px-14">ยี่ห้อรถ</p>
            <input
              type="text"
              className="py-3 px-5 rounded-md text-black outline-none text-2xl text-center"
              value={carBrand}
              onChange={(e) => setCarBrand(e.target.value)}
              placeholder="xxxxxxxxxx"
            />
          </div>
          <div className="flex py-8 justify-between mr-14">
            <p className="text-5xl text-center text-gray-50 px-14">รุ่นรถ</p>
            <input
              type="text"
              className="py-3 px-5 rounded-md text-black outline-none text-2xl text-center"
              value={carModel}
              onChange={(e) => setCarModel(e.target.value)}
              placeholder="xxxxxxxxxx"
            />
          </div>
          <div className="flex justify-between mr-14">
            <p className="text-5xl text-center text-gray-50 px-14">หมายเหตุ</p>
            <input
              type="text"
              className="py-3 px-5 rounded-md text-black outline-none text-2xl text-center"
              value={note}
              onChange={(e) => setNote(e.target.value)}
              placeholder="xxxxxxxxxx"
            />
          </div>
          <div className="flex justify-center items-center mt-8 mb-8 gap-20">
            <button
              type="button"
              onClick={handleAddList}
              className="bg-yellow-400 hover:bg-yellow-600 text-slate-800 text-xl font-bold font-serif py-3 px-14 border border-yellow-600 rounded"
            >
              ADD
            </button>
            <button
              onClick={() => setShowModal(true)}
              className="bg-yellow-400 hover:bg-yellow-600 text-slate-800 text-xl font-bold font-serif py-3 px-14 border border-yellow-600 rounded"
            >
              DATA
            </button>
          </div>
        </div>
      </div>

      <Modal isVisible={showModal} onClose={() => setShowModal(false)}>
        <div className="rounded h-[400px] w-[1240px] bg-gray-800 overflow-auto ">
          <table className="min-w-full table-fixed">
            <thead className="font-normal sticky top-0">
              <tr className="border-solid bg-gradient-to-r from-cyan-500 to-blue-500">
                {headers.map((header, index) => (
                  <th
                    className="px-5 py-2 border-solid border-2 border-primary"
                    key={index}
                  >
                    <span className="text-left text-white text-2xl whitespace-nowrap">
                      {header.key}
                    </span>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {allList.map((item, index) => (
                <tr key={index}>
                  <td className="px-2 py-2 border-solid border-2 border-primary text-center text-white text-xl">
                    <p>{item.regis}</p>
                  </td>
                  <td className="px-2 py-2 border-solid border-2 border-primary text-center  text-white text-xl">
                    <p>{item.brand}</p>
                  </td>
                  <td className="px-2 py-2 border-solid border-2 border-primary text-center  text-white text-xl">
                    <p>{item.model}</p>
                  </td>
                  <td className="px-2 py-2 border-solid border-2 border-primary text-center  text-white text-xl">
                    <p>{item.noted}</p>
                  </td>
                  <span
                    onClick={() => handleDeleteList(index)}
                    className="w-6 h-6 inline-flex absolute cursor-pointer items-center justify-center bg-red-200 rounded-full text-red-500"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="w-6 h-6"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                      />
                    </svg>
                  </span>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </Modal>
    </div>
  );
};

export default App;
