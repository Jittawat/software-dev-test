import React from "react";

const Modal = ({ isVisible, onClose, children }) => {
  if (!isVisible) return null;

  const handleClose = (e) => {
    if (e.target.id === "wrapper") onClose();
  };

  return (
    <div
      className="fixed inset-0 bg-black bg-opacity-25 backdrop-blur-sm flex justify-center items-center pt-16"
      id="wrapper"
      onClick={handleClose}
    >
      <div className="flex flex-col">
        {children}
        <div className="flex justify-center items-center my-2">
          <button
            className=" px-4 py-4 rounded-md mx-2 focus:outline-none bg-white text-gray-700 hover:bg-red-600 hover:text-white"
            onClick={() => onClose()}
          >
            Close
          </button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
